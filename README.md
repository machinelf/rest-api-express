# REST API Boilerplate

NodeJS Express boilerplate for easily spinning up a REST API. Written in ES6.

## To use
- Clone the repository
- `cd rest-api` and `npm i`
- Run `npm start`

Use cURL or POSTMAN to interact with the API to see example responses.

Add your MongoDB url to the `index.js` file and follow the instructions included on `line 11` to start using the database.

## Frameworks/libraries
- [Express](https://expressjs.com/)
- [Body Parser](https://www.npmjs.com/package/body-parser)
- [Mongoose](https://mongoosejs.com/)
