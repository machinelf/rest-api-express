import mongoose from 'mongoose'
import itemSchema from '../models/item.model'
 
const Item = mongoose.model('Item', itemSchema)
 
// add new item to the database
export function addNewItem(req, res) {
    let newItem = new Item(req.body)
    newItem.save((error, item) => {
        if (error) { res.json(error) }
        res.json(item)
    })
}
 
// get all items from the database
export function getItems(req, res) {
    Item.find({}, (error, items) => {
        if (error) { res.json(error) }
        res.json(items)
    })
}
 
// get single item based on the id
export function getItem(req, res) {
    Item.findById(
        req.params.id, 
        (error, item) => {
            if (error) { res.json(error) }
            res.json(item)
        }
    )
}
 
// update the item information based on id
export function updateItem(req, res) {
    Item.findOneAndUpdate(
        { _id: req.params.id }, 
        req.body, 
        { new: true }, 
        (error, item) => {
            if (error) { res.json(error) }
            res.json(item)
        }
    )
}
 
// delete the item from the database.
export function deleteItem(req, res) {
    Item.remove(
        { _id: req.params.id }, 
        (error, item) => {
            if (error) { res.json(error) }
            res.json(item)
        }
    )
}