// Test Controller file 

import mongoose from 'mongoose'
import itemSchema from '../models/item.model'
 
const Item = mongoose.model('Item', itemSchema)
 
// add new item to the database
export function addNewItem(req, res) {
    res.status(200).send({
        status: 'success',
        added: {
            name: req.body.name
        }
    })
}
 
// get all items from the database
export function getItems(req, res) {
    res.status(200).send({
        status: 'success',
        items: [
            {
                name: 'Test'
            }
        ]
    })
}
 
// get single item based on the id
export function getItem(req, res) {
    res.status(200).send({
        status: 'success',
        item: {
            id: req.params.id,
            name: 'Test'
        }
    })
}
 
// update the item information based on id
export function updateItem(req, res) {
    res.status(200).send({
        status: 'success',
        updated: {
            id: req.params.id,
            name: req.body.name
        }
    })
}
 
// delete the item from the database.
export function deleteItem(req, res) {
    res.status(200).send({
        status: 'success',
        deleted: {
            id: req.params.id,
            name: 'Test'
        }
    })
}