import express from 'express'
import itemRoutes from './routes/item.routes'
import mongoose from 'mongoose'
import bodyParser from 'body-parser'
 
const app = express()
const PORT = 3000
 
mongoose.Promise = global.Promise;

// Add your mongo url here, then remove '.test' from line 8 in routes/item.routes.js
// to use the connected controller

// mongoose.connect('mongodb://user:password@ds239648.mlab.com:39648/db_name')
 
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());
 
app.get('/', (req, res) => {
    res.send("Welcome to the REST API")
})
itemRoutes(app)
 
app.listen(PORT, () => {
    console.log(`Server is running on ${PORT}`);
})