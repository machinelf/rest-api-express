import mongoose from 'mongoose'
 
const Schema = mongoose.Schema
 
const ItemSchema = new Schema({
    name: {
        type: String,
        required: 'Name required'
    },
})
 
export default ItemSchema;