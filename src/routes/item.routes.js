
import { 
    addNewItem, 
    getItems, 
    getItem, 
    updateItem, 
    deleteItem 
} from '../controllers/item.controller.test'
 
export default (app) => {
    app.route('/items')
    .get(getItems)
    .post(addNewItem)
 
    app.route('/items/:id')
    .get(getItem)
    .put(updateItem)
    .delete(deleteItem)
}